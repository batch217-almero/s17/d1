//console.log("Hello World");

/*Function syntax
function functionName(parameter) {
	code block (statement)
}
*/

// DECLARE FUNCTION
function printName() {
	console.log("My name is John");
}

// CALL-INVOKE FUNCTION
printName();

function declaredFunction(){
	console.log("Hello World from declaredFunction");
}

declaredFunction();

// FUNCTION EXPRESSION - Function stored inside a variable
let variableFunction = function(){
	console.log("Hello Again!");
}
// CALL-INVOKE FUNCTION EXPRESSION
variableFunction();

// If a function is stored in a variable, you cannot call it w/out using the variable storage
let funcExpression = function funcName(){
	console.log("Hello from the other side.")
}

funcExpression();

funcExpression = function(){
	console.log("updated funcExpression");
}
funcExpression();
funcExpression();

// RE-ASSIGN DECLARED FUNCTION
declaredFunction = function(){
	console.log("Updated declaredFunction");
}

declaredFunction();

// RE-ASSIGNING CONST FUNCTIONS
const constantFunc = function(){
	console.log("Initialized with const!");
}

constantFunc();
/*// Re-assign const
constantFunc = function(){
	console.log("Can we re-assign it?");
}

constantFunc();*/

// FUNCTION SCOPING (Local, Global, Function)

// Local/Block Scope Sample
{
	let	localVar = "Armando Perez";
	console.log(localVar);
}

// Global Scope sample
let globalScope = "Mr. Worldwide";
console.log(globalScope);

// Function Scope Sample
function showNames(){
	// Function scoped variables:
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet);
}

showNames();


//NESTED FUNCTION
function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}

myNewFunction();

// Function and Global Scoped Variable
// Global Scoped variable

let globalName = "Alexandro";

function myNewFunction2(){
	let nameInside = "Renz";

	console.log(globalName);
}

myNewFunction2();

// USING ALERT WINDOWS WITH FUNCTIONS
alert("Hello World!");

function showAlert() {
	alert("Hello user");
}

showAlert();

console.log("I will only log in the console when alert is dismissed.");

// USING PROMPT WINDOWS WITH FUNCTIONS
// Propmt can be used for gathering user input
let samplePrompt = prompt("Enter your name.");
console.log("Hello, " + samplePrompt);
console.log(typeof samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter your last name.");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();

// FUNCTION NAMING CONVENTIONS (How to Name Functions)
// Inlcude Action Verb

function getCourse(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}

getCourse();
